public class Main {

    public static void main(String[] args){
        Stack stack = new Stack();
        System.out.println(stack.get());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        System.out.println(stack.get());
        stack.pop();
        System.out.println(stack.get());
    }

}
